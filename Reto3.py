import cv2
import pandas as pd
import numpy as np
from sklearn.cluster import KMeans
from os import listdir

def ls(ruta):
    return listdir(ruta)
def siff():
    for  folders in ls("Reto3"):
        for files in ls("Reto3" + "/"+ folders):
            print(files)
            img = cv2.imread("Reto3/" + folders + "/" + files)
            gray= cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
            sift = cv2.xfeatures2d.SIFT_create()
            kp, des = sift.detectAndCompute(gray,None)
            index = ["sift" + str(i) for i in range(1, len(des)+1)]
            df = pd.DataFrame(des, index=index)
            temp = len(files)
            df.to_csv("Reto3/" + folders + "/" + files[:temp-3] + "csv", index=False)
            #file= open("Reto3/" + folders + "/"  + files[:temp-3] + "txt", "w")
            #for sift in des:

def kmeans():
    cont = 0
    j = 1
    k=500
    dir="histograma4"
    # for  folders in ls("Reto3"):
    #     dictLabelBloques[folders] = j
    for folders in ls("folders"):
        files =ls("folders/"+folders+"/csv")
        for i in range(0,len(files)):
            siff = files[i]
            df = pd.read_csv("folders/"+folders+"/csv/"+files[i])
            if len(df.values) > k:
                kmeans = KMeans(n_clusters=k, random_state=0)
                labels = kmeans.fit_predict(df.values)
                quant = kmeans.cluster_centers_.astype("uint8")[labels]
                temporalDictLabel = {}
                temporalDict = {}
                for i in range(0,len(labels)):
                    if labels[i] in temporalDictLabel:
                        temporalDictLabel[labels[i]] +=1
                    else:
                        temporalDict[labels[i]] = quant[i]
                        temporalDictLabel[labels[i]] =1
                
                
                m = []
                cont += 1
                print("Imagenes analizadas" + str(cont))
                for key in temporalDictLabel:
                    
                    m2 = []
                    m2.append(j)
                    m2.append(temporalDictLabel[key])
                    for sift in temporalDict[key]:
                        m2.append(sift)
                    m.append(m2)
                
                df = pd.DataFrame(m)
                temp = len(siff)
                print("Generando "+dir+"/"+str(folders) + str(siff[:temp-3]) + "csv")
                print("En la carpeta" + str(folders))
                df.to_csv(dir+"/" +folders + siff[:temp-3] + "csv", index=False)
        j +=1
kmeans()
#ift = cv2.xfeatures2d.SIFT_create()quant   
