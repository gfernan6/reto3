import cv2
import pandas as pd
import numpy as np
from sklearn.cluster import KMeans
from sklearn import model_selection
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
from sklearn.metrics import confusion_matrix
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.externals import joblib
from sklearn import svm
from flask import Flask
app = Flask(__name__)
 
@app.route("/")
def index():
	places =  ["18","19","26","38","admisiones","agora","auditorio","biblioteca", "dogger", "idiomas"]
	img = cv2.imread("imActual.jpg")
     	img = cv2.resize(img, (1280, 720)) 
	gray= cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
	sift = cv2.xfeatures2d.SIFT_create()
	kp, des = sift.detectAndCompute(gray,None)
	kmeans = KMeans(n_clusters=400, random_state=0)
	labels = kmeans.fit_predict(des)
	quant = kmeans.cluster_centers_.astype("uint8")[labels]
	temporalDictLabel = {}
        temporalDict = {}
        for i in range(0,len(labels)):
            if labels[i] in temporalDictLabel:
                temporalDictLabel[labels[i]] +=1
            else:
                temporalDict[labels[i]] = quant[i]
                temporalDictLabel[labels[i]] =1
        m = []
        for key in temporalDictLabel:
            m2 = []
            m2.append(temporalDictLabel[key])
            for sift in temporalDict[key]:
                m2.append(sift)
            m.append(m2)
	p = []
        matrixFinal = np.array(m).flatten()
	p.append(matrixFinal)
	print(p)
        clf = joblib.load('model3.pkl')          
        prediction = clf.predict(p)
        return str(places[int(prediction[0])])
 
if __name__ == "__main__":
        app.run(host='0.0.0.0', debug=True)
