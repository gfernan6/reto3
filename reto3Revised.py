import cv2
import pandas as pd
import numpy as np
from sklearn.cluster import KMeans
from sklearn import model_selection
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
from sklearn.metrics import confusion_matrix
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.externals import joblib
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn import svm
from os import listdir

def ls(ruta):
    return listdir(ruta)
def siff():
    for  folders in ls("Reto3"):
        for files in ls("Reto3" + "/"+ folders):
            print(files)
            img = cv2.imread("Reto3/" + folders + "/" + files)
            gray= cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
            sift = cv2.xfeatures2d.SIFT_create()
            kp, des = sift.detectAndCompute(gray,None)
            index = ["sift" + str(i) for i in range(1, len(des)+1)]
            df = pd.DataFrame(des, index=index)
            temp = len(files)
            df.to_csv("Reto3/" + folders + "/" + files[:temp-3] + "csv", index=False)
            #file= open("Reto3/" + folders + "/"  + files[:temp-3] + "txt", "w")
            #for sift in des:

def kmeans():
    cont = 0
    j = 8
    dictLabelBloques = {}
    folders = "csv2"
    
    # for  folders in ls("Reto3"):
    #     dictLabelBloques[folders] = j
    files =ls("csv2")
    
    for i in range(59,len(files)):
        siff = files[i]
        df = pd.read_csv("csv2/"+files[i])
        print(siff)
        kmeans = KMeans(n_clusters=250, random_state=0)
        labels = kmeans.fit_predict(df.values)
        quant = kmeans.cluster_centers_.astype("uint8")[labels]
        temporalDictLabel = {}
        temporalDict = {}
        for i in range(0,len(labels)):
            if labels[i] in temporalDictLabel:
                temporalDictLabel[labels[i]] +=1
            else:
                temporalDict[labels[i]] = quant[i]
                temporalDictLabel[labels[i]] =1
        
        
        m = []
        cont += 1
        print("Imagenes analizadas: " + str(cont))
        for key in temporalDictLabel:
            
            m2 = []
            m2.append(j)
            m2.append(temporalDictLabel[key])
            for sift in temporalDict[key]:
                m2.append(sift)
            m.append(m2)
        
        df = pd.DataFrame(m)
        temp = len(siff)
        print("Generando histogramas: "+str(folders) + str(siff[:temp-3]) + "csv")
        print("En la carpeta" + str(folders))
        df.to_csv("histograma/auditorio" + siff[:temp-3] + "csv", index=False)

def oneLine():
    folder = "histograma4"
    matrixtoDF = []
    arrayLabels = []
    for csv in ls(folder):
        #print(csv)
        df = pd.read_csv(folder+"/"+csv)
        matrizTemp = df.values
        labelY = matrizTemp[0][0]
        arrayLabels.append(labelY)
        #filelabels.write(str(labelY))
        df = df.drop('0', 1) 
        dfValues = df["1"] 
        #total = np.sum(dfValues)
        #dfValues = dfValues/total
        
        df = df.drop('1', 1) 
        df = df/180
        matrizToArray = df.values        
        matrixFinal = matrizToArray.flatten()
        matrixtoDF.append(matrixFinal)
        #fileFinal.write(matrixtoDF)
        #print(matrixtoDF)
        
        
    print(arrayLabels)
    print(matrixtoDF)
    df = pd.DataFrame(matrixtoDF)
    df2 = pd.DataFrame(arrayLabels)
    df.to_csv("model4.csv", index=False)
    df2.to_csv("labels4.csv", index=False)
oneLine()
def suport():
    dfX = pd.read_csv("model4.csv")
    dfY = pd.read_csv("labels4.csv")
    print(dfY.values.flatten())
    X_train, X_test, y_train, y_test = train_test_split(dfX.values, dfY.values.flatten(), test_size=0.1, random_state=1)
    print(len(X_train))
    X_train, X_val, y_train, y_val = train_test_split(X_train, y_train, test_size=0.2, random_state=1)
    print(len(X_train))
    #clf = KNeighborsClassifier(n_neighbors=10)
    clf = svm.SVC()
    #clf = MLPClassifier(solver='lbfgs', random_state=1)
    clf.fit(X_train, y_train)  
    joblib.dump(clf, 'model4.pkl') 
    #clf = joblib.load('model.pkl') 
    predictions = clf.predict(X_val)
    print(predictions)
    print(accuracy_score(y_val, predictions))
    print(confusion_matrix(y_val, predictions))
    
suport()


#ift = cv2.xfeatures2d.SIFT_create()quant   
